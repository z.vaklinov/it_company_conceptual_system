﻿using CS.Core.Application.Models.Abstractions;
using FluentValidation;

namespace CS.Core.Application.Services
{
    public class ApplicationValidationService<TValidatable> : AbstractValidator<TValidatable>
        where TValidatable : Validatable
    {
        public ApplicationValidationService()
        {
            CascadeMode = CascadeMode.Stop;
        }
    }
}
