﻿using CS.Core.Application.Models;
using FluentValidation;

namespace CS.Core.Application.Services
{
    internal class ContractorValidationService : ApplicationValidationService<Contractor>
    {
        public ContractorValidationService()
        {
            RuleFor(x => x.FirstName).NotEmpty().Length(3, 15);
            RuleFor(x => x.LastName).NotEmpty().Length(3, 15);
            RuleFor(x => x.Salary).NotEmpty();
            RuleFor(x => x.HireDate).NotEmpty();
        }
    }
    
}
