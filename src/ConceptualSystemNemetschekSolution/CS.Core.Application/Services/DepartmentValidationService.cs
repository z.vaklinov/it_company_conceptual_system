﻿using CS.Core.Application.Models;
using FluentValidation;

namespace CS.Core.Application.Services
{
    internal class DepartmentValidationService : ApplicationValidationService<Department>
    {
        public DepartmentValidationService()
        {
            RuleFor(x => x.Name).NotEmpty().Length(10, 30);
        }
    }
}
