﻿using CS.Core.Application.Models;
using FluentValidation;

namespace CS.Core.Application.Services
{
    internal class ProjectValidationService : ApplicationValidationService<Project>
    {
        public ProjectValidationService()
        {
            RuleFor(x => x.Name).NotEmpty().Length(10, 30);
            RuleFor(x => x.Description).NotEmpty().Length(20,100);
            RuleFor(x => x.StartDate).NotEmpty().GreaterThanOrEqualTo(DateTime.Now);
            RuleFor(x => x.EndDate).GreaterThan(x => x.StartDate);
        }
    }
}
