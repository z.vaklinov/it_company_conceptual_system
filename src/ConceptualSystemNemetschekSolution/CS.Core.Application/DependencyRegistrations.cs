﻿using CS.Core.Application.Models;
using Microsoft.Extensions.DependencyInjection;

namespace CS.Core.Application
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterApplicationDependencies(
            this IServiceCollection services)
        {
            services.AddScoped(p => new ApplicationContext(new CurrentUser(Guid.NewGuid())));

            return services;
        }
    }
}
