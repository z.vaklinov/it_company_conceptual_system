﻿
namespace CS.Core.Application.Exceptions
{
    public class DomainException : Exception
    {
        public DomainException(string propertyName, string message)
            : base(message)
        {
            PropertyName = propertyName;
        }

        public string PropertyName { get; }
    }
}
