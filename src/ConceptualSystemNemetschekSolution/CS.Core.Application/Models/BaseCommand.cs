﻿using MediatR;

namespace CS.Core.Application.Models
{
    public abstract class BaseCommand<TResult> : IRequest<TResult>
    {
    }
}
