﻿using MediatR;

namespace CS.Core.Application.Models
{
    public abstract class BaseQuery<TResult> : IRequest<TResult>
    {
    }
}
