﻿using CS.Core.Application.Contracts;
using FluentValidation;

namespace CS.Core.Application.Models.Abstractions
{
    public abstract class Entity<TIdentifier> :
       Validatable,
       IIdentifiable<TIdentifier>,
       IModifiable
    {

        protected Entity(
            Func<TIdentifier> identifierFactory,
            Func<IValidator> validatorFactory = null)
            : this(validatorFactory)
        {
            Id = identifierFactory();
        }

        protected Entity(Func<IValidator> validatorFactory = null)
            : base(validatorFactory)
        {
            CreatedOn = DateTimeOffset.UtcNow;
            MarkAsModified(CreatedOn);
        }


        public TIdentifier Id { get; }

        public DateTimeOffset CreatedOn { get; }

        public DateTimeOffset ModifiedOn { get; }


        public void MarkAsModified(DateTimeOffset? modificationTime = null)
        {
            if (modificationTime == null)
            {
                modificationTime = DateTimeOffset.UtcNow;
            }
            else
            {
                modificationTime = modificationTime.Value;
            }
        }
    }
}
