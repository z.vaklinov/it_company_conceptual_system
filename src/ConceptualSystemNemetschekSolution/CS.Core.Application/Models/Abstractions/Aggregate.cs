﻿using FluentValidation;

namespace CS.Core.Application.Models.Abstractions
{
    public abstract class Aggregate<TIdentifier> : Entity<TIdentifier>
    {
        protected Aggregate(
            Func<TIdentifier> identifierFactory,
            Func<IValidator> validatorFactory = null)
            : base(identifierFactory, validatorFactory)
        {

        }

        protected Aggregate(Func<IValidator> validatorFactory = null)
            : base(validatorFactory)
        {

        }
    }
}
