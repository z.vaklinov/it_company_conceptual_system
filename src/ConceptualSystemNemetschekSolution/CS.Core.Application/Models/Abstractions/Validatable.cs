﻿using CS.Core.Application.Exceptions;
using CS.Core.Application.Services;
using FluentValidation;
using FluentValidation.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Core.Application.Models.Abstractions
{
    public abstract class Validatable
    {
        private readonly Func<IValidator> _validatorFactory;

        public Validatable(Func<IValidator> validatorFactory = null)
        {
            _validatorFactory = validatorFactory ?? CreateDefault;
        }

        protected void ValidateProperty(string property)
        {
            var context = new ValidationContext<Validatable>(
                this,
                new PropertyChain(),
                new MemberNameValidatorSelector(new[] { property }));

            var validator = _validatorFactory();
            var result = validator.Validate(context);

            if (result.IsValid == false)
            {
                var error = result.Errors.FirstOrDefault();
                throw new DomainException(error.PropertyName, error.ErrorMessage);
            }
        }

        private IValidator CreateDefault()
        {
            return new ApplicationValidationService<Validatable>();
        }
    }
}
