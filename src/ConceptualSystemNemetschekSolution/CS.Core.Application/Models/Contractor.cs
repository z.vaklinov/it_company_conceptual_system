﻿using CS.Core.Application.Enums;
using CS.Core.Application.Models.Abstractions;
using CS.Core.Application.Services;


namespace CS.Core.Application.Models
{
    public class Contractor : Entity<Guid>
    {
        private Guid _departmentId;
        private Guid _projectId;
        private Guid _managerId;
        private string _firstName;
        private string _lastName;
        private Positions _position;
        private decimal _salary;
        private DateTimeOffset _hireDate;

        private readonly List<Project> _projects;

        public Contractor(Guid departmentId, Guid projectId, Guid managerId, string firstName, string lastName,
            Positions position, decimal salary, DateTimeOffset hireDate) : this()
        {
            DepartmentId = departmentId;
            ProjectId = projectId;
            ManagerId = managerId;
            FirstName = firstName;
            LastName = lastName;
            Position = position;
            Salary = salary;
            HireDate = hireDate;
        }

        protected Contractor()
            : base(() => new ContractorValidationService())
        {
            _projects = new List<Project>();
        }

        public Guid DepartmentId
        {
            get => _departmentId;
            private set
            {
                _departmentId = value;
                ValidateProperty(nameof(DepartmentId));
            }
        }

        public Guid ProjectId
        {
            get => _projectId;
            private set
            {
                _projectId = value;
                ValidateProperty(nameof(ProjectId));
            }
        }


        public Guid ManagerId
        {
            get => _managerId;
            private set
            {
                _managerId = value;
                ValidateProperty(nameof(ManagerId));
            }
        }

        public string FirstName
        {
            get => _firstName;
            private set
            {
                _firstName = value;
                ValidateProperty(nameof(FirstName));
            }
        }

        public string LastName
        {
            get => _lastName;
            private set
            {
                _lastName = value;
                ValidateProperty(nameof(LastName));
            }
        }

        public Positions Position
        {
            get => _position;
            private set
            {
                _position = value;
                ValidateProperty(nameof(Position));
            }
        }

        public decimal Salary
        {
            get => _salary;
            private set
            {
                _salary = value;
                ValidateProperty(nameof(Salary));
            }
        }

        public DateTimeOffset HireDate
        {
            get => _hireDate;
            private set
            {
                _hireDate = DateTimeOffset.Now;
                ValidateProperty(nameof(HireDate));
            }
        }

        public IReadOnlyCollection<Project> Projects => _projects;

    }
}
