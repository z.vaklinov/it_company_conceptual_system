﻿using CS.Core.Application.Models.Abstractions;
using CS.Core.Application.Services;

namespace CS.Core.Application.Models
{
    public class Project : Entity<Guid>
    {
        private Guid _contractorId;
        private string _name;
        private string _description;
        private DateTime _startDate;
        private DateTime _endDate;

        private readonly List<Contractor> _contractors;

        public Project(Guid contractorId, string name, string description, DateTime startDate, DateTime endDate)
            : this()
        {
            ContractorId = contractorId;
            Name = name;
            Description = description;
            StartDate = startDate;
            EndDate = endDate;
        }

        protected Project()
           : base(() => new ProjectValidationService())
        {
            _contractors = new List<Contractor>();
        }

        public Guid ContractorId
        {
            get => _contractorId;
            private set
            {
                _contractorId = value;
                ValidateProperty(nameof(ContractorId));
            }

        }

        public string Name
        {
            get => _name;
            private set
            {
                _name = value;
                ValidateProperty(nameof(Name));
            }
        }

        public string Description
        {
            get => _description;
            private set
            {
                _description = value;
                ValidateProperty(nameof(Description));
            }
        }

        public DateTime StartDate
        {
            get => _startDate;
            private set
            {
                _startDate = value;
                ValidateProperty(nameof(StartDate));
            }
        }

        public DateTime EndDate
        {
            get => _endDate;
            private set
            {
                _endDate = value;
                ValidateProperty(nameof(EndDate));
            }
        }

        public IReadOnlyCollection<Contractor> Contractors => _contractors;
    }
}
