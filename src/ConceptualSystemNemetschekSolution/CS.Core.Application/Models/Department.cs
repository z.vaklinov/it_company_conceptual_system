﻿using CS.Core.Application.Models.Abstractions;
using CS.Core.Application.Services;

namespace CS.Core.Application.Models
{
    public class Department : Entity<Guid>
    {
        private string _name;
        private Guid _contractorId;

        private readonly List<Contractor> _contractors;

        public Department(string name, Guid contractorId) : this()
        {
            Name = name;
            ContractorId = contractorId;
        }

        protected Department()
           : base(() => new DepartmentValidationService())
        {
            _contractors = new List<Contractor>();
        }

        public Guid ContractorId
        {
            get => _contractorId;
            private set
            {
                _contractorId = value;
                ValidateProperty(nameof(ContractorId));
            }

        }

        public string Name
        {
            get => _name;
            private set
            {
                _name = value;
                ValidateProperty(nameof(Name));
            }
        }

        public IReadOnlyCollection<Contractor> Contractors => _contractors;

    }
}
