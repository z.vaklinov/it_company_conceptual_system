﻿
namespace CS.Core.Application.Enums
{
    public enum Positions
    {
        Unassigned = 0,

        JuniorSoftwareEngineer = 1,

        RegularSoftwareEngineer = 2,

        SeniorSoftwareEngineer = 3,

        TechLead = 4,

        SoftwareArchitect = 5,

        ProjectManager = 6,

        QualityAssuranceTechnician = 7,

        MarketingAssistant = 8,

        HumanResourcesAssistant = 9,

        SalesRepresentative = 10,

        AdministrationManager = 11,

        ChiefExecutiveOffice = 12

    }
}
