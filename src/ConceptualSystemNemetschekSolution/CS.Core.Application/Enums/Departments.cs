﻿
namespace CS.Core.Application.Enums
{
    public enum  Departments
    {
        Unassigned = 0,

        Engineering = 1,

        QualityAssurance = 2,

        Marketing = 3,

        HumanResources = 4,

        Sales = 5,

        Administration = 6,

        Executive = 7

    }
}
