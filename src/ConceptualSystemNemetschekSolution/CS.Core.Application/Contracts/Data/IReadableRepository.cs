﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Core.Application.Contracts.Data
{
    public interface IReadableRepository<TIdentificator>
        where TIdentificator : notnull
    {
        Task<IIdentifiable<TIdentificator>> GetSingleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null);

        Task<IReadOnlyCollection<IIdentifiable<TIdentificator>>> GetMultipleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null);
    }
}
