﻿using CS.Core.Application.Models.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Core.Application.Contracts.Data
{
    public interface IMutatableRepository<TAggregate> : IReadableRepository<Guid>
        where TAggregate : Aggregate<Guid>
    {
        Task<TAggregate> AddAsync(TAggregate aggregate);

        Task<IReadOnlyCollection<TAggregate>> AddAsync(IReadOnlyCollection<TAggregate> aggregates);

        Task<TAggregate> UpdateAsync(TAggregate aggregate);

        Task<TAggregate> DeleteAsync(TAggregate aggregate);
    }
}
