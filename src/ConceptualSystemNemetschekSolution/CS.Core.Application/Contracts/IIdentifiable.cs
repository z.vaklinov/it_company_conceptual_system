﻿
namespace CS.Core.Application.Contracts
{
    public interface IIdentifiable<TIdentificator>
    {
        TIdentificator Id { get; }
    }
}
