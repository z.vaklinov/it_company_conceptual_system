﻿
namespace CS.Core.Application.Contracts
{
    public interface IModifiable
    {
        DateTimeOffset CreatedOn { get; }

        DateTimeOffset ModifiedOn { get; }

        void MarkAsModified(DateTimeOffset? modificationTime = null);
    }
}
