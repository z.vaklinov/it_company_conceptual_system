﻿using CS.Core.Application.Contracts;
using CS.Core.Application.Contracts.Data;
using Microsoft.EntityFrameworkCore;

namespace CS.Infrastructure.Persistence.MsSQL.Repositories
{
    internal class ReadableRepository<TIdentificator> : IReadableRepository<TIdentificator>
        where TIdentificator : notnull
    {
        protected readonly MsSQLDbContext _dbContext;

        public ReadableRepository(MsSQLDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IIdentifiable<TIdentificator>> GetSingleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null)
        {
            var query = _dbContext
                .Set<IIdentifiable<TIdentificator>>()
                .AsQueryable();

            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }

            var result = query
                .Where(predicate)
                .FirstOrDefault();

            return result;
        }

        public Task<IReadOnlyCollection<IIdentifiable<TIdentificator>>> GetMultipleAsync(
            Func<IIdentifiable<TIdentificator>, bool> predicate,
            IReadOnlyCollection<string>? includes = null)
        {
            throw new NotImplementedException();
        }
    }
}
