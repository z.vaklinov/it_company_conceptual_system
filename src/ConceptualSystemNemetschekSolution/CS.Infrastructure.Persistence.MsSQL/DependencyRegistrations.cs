﻿using CS.Core.Application.Contracts.Data;
using CS.Infrastructure.Persistence.MsSQL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CS.Infrastructure.Persistence.MsSQL
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterMsSQLDependencies(
            this IServiceCollection services,
            string mssqlConnectionString)
        {
            services.AddDbContext<MsSQLDbContext>(
                builder => builder.UseSqlServer(
                    mssqlConnectionString,
                    opt => opt.MigrationsAssembly(typeof(MsSQLDbContext).Assembly.GetName().Name)));

            services.AddTransient(typeof(IReadableRepository<>), typeof(ReadableRepository<>));
            services.AddTransient(typeof(IMutatableRepository<>), typeof(MutatableRepository<>));

            return services;
        }
    }
}
