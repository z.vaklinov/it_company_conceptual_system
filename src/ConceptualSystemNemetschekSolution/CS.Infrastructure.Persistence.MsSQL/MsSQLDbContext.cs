﻿using CS.Core.Application.Models;
using CS.Infrastructure.Persistence.MsSQL.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Data;

namespace CS.Infrastructure.Persistence.MsSQL
{
    public class MsSQLDbContext : DbContext
    {
        private IDbContextTransaction? _currentTransaction;

        public MsSQLDbContext(DbContextOptions<MsSQLDbContext> options)
            : base(options)
        {
        }

        public DbSet<Contractor> Contractors { get; }
        public DbSet<Department> Departments { get; }
        public DbSet<Project> Projects { get; }

        public bool HasActiveTransaction => _currentTransaction != null;

        public async Task<IDbContextTransaction> BeginTransactionAsync(
            IsolationLevel isolationLevel,
            CancellationToken cancellationToken = default)
        {
            if (HasActiveTransaction)
            {
                throw new Exception("There is an active transaction that has not been finished.");
            }

            _currentTransaction = await Database.BeginTransactionAsync(isolationLevel, cancellationToken);

            return _currentTransaction;
        }

        public async Task CommitTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (HasActiveTransaction == false)
            {
                throw new Exception("There is no active transaction.");
            }

            try
            {
                await SaveChangesAsync(cancellationToken);
                await _currentTransaction.CommitAsync(cancellationToken);
            }
            catch (Exception)
            {
                await _currentTransaction.RollbackAsync(cancellationToken);
                throw;
            }
            finally
            {
                await _currentTransaction.DisposeAsync();
                _currentTransaction = null;
            }
        }

        public async Task RollbackTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (HasActiveTransaction)
            {
                try
                {
                    await _currentTransaction.RollbackAsync(cancellationToken);
                }
                finally
                {
                    await _currentTransaction.DisposeAsync();
                    _currentTransaction = null;
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ContractorConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DepartmentConfiguration).Assembly);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProjectConfiguration).Assembly);


          // modelBuilder.Entity<Contractor>()
          //     .HasOne<Department>()
          //     .WithOne();
          //
          // modelBuilder.Entity<Department>()
          //     .HasMany<Contractor>()
          //     .WithOne();
          //
          //  modelBuilder.Entity<Project>()
          //      .HasMany<Contractor>();
          // 
          //  modelBuilder.Entity<Contractor>()
          //      .HasMany<Project>();
        }
    }
}