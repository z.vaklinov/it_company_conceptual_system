﻿using CS.Core.Application.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS.Infrastructure.Persistence.MsSQL.Configurations
{
    internal class DepartmentConfiguration : BaseEntityConfiguration<Department>
    {
        public override void Configure(EntityTypeBuilder<Department> builder)
        {
            base.Configure(builder);

            builder
                .HasMany(x => x.Contractors)
                .WithOne()
                .HasPrincipalKey(p => p.Id)
                .HasForeignKey(c => c.DepartmentId);
        }
    }
}
