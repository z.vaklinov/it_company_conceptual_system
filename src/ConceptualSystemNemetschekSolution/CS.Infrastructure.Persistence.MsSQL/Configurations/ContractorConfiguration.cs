﻿using CS.Core.Application.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CS.Infrastructure.Persistence.MsSQL.Configurations
{
    internal class ContractorConfiguration : BaseEntityConfiguration<Contractor>
    {
        public override void Configure(EntityTypeBuilder<Contractor> builder)
        {
            base.Configure(builder);

            builder
                .HasMany(x => x.Projects)
                .WithOne()
                .HasPrincipalKey(p => p.Id)
                .HasForeignKey(c => c.ContractorId);

            

        }

    }
}
