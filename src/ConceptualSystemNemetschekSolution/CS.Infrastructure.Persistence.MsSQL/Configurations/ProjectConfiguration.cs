﻿using CS.Core.Application.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace CS.Infrastructure.Persistence.MsSQL.Configurations
{
    internal class ProjectConfiguration : BaseEntityConfiguration<Project>
    {
        public override void Configure(EntityTypeBuilder<Project> builder)
        {
            base.Configure(builder);

            builder
                .HasMany(x => x.Contractors)
                .WithOne()
                .HasPrincipalKey(p => p.Id)
                .HasForeignKey(c => c.ProjectId);
        }
    }
}
