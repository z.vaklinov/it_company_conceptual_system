﻿using CS.Core.Application.Contracts.Data;
using CS.Infrastructure.Persistence.MsSQL;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace CS.Infrastructure.Persistence
{
    public static class DependencyRegistrations
    {
        public static IServiceCollection RegisterPersistenceDependencies(
            this IServiceCollection services,
            string mssqlConnectionString)
        {
            services.RegisterMsSQLDependencies(mssqlConnectionString);

            services.AddTransient<IDataUnitOfWork, DataUnitOfWork>();
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionPipelineService<,>));

            return services;
        }
    }
}
