﻿using CS.Core.Application.Contracts.Data;
using CS.Infrastructure.Persistence.MsSQL;
using System.Data;

namespace CS.Infrastructure.Persistence
{
    internal class DataUnitOfWork : IDataUnitOfWork
    {
        private readonly MsSQLDbContext _msSQLDbContext;

        public DataUnitOfWork(MsSQLDbContext msSQLDbContext)
        {
            _msSQLDbContext = msSQLDbContext;
        }

        public bool HasActiveTransaction => _msSQLDbContext.HasActiveTransaction;

        public async Task BeginTransactionAsync(
            IsolationLevel isolationLevel,
            CancellationToken cancellationToken = default)
        {
            var transaction = await _msSQLDbContext.BeginTransactionAsync(isolationLevel, cancellationToken);
        }

        public async Task CommitTransactionAsync(CancellationToken cancellationToken = default)
        {
            await _msSQLDbContext.CommitTransactionAsync(cancellationToken);
        }

        public async Task RollbackTransactionAsync(CancellationToken cancellationToken = default)
        {
            await _msSQLDbContext.RollbackTransactionAsync(cancellationToken);
        }
    }
}